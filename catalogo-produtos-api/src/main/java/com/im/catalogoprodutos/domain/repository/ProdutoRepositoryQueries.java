package com.im.catalogoprodutos.domain.repository;

import java.math.BigDecimal;
import java.util.List;

import com.im.catalogoprodutos.domain.model.Produto;

public interface ProdutoRepositoryQueries {

	List<Produto> consultaComFiltros(String nomeOuDescricao, BigDecimal precoMinimo, BigDecimal precoMaximo);
	
}