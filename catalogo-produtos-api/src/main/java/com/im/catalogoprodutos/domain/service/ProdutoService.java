package com.im.catalogoprodutos.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.im.catalogoprodutos.domain.model.Produto;
import com.im.catalogoprodutos.domain.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Produto salvar(Produto produto) {
		return produtoRepository.save(produto);
	}
	
	public void excluir(Long id) {
		produtoRepository.deleteById(id);
	}
	
}
