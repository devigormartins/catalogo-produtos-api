package com.im.catalogoprodutos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatalogoProdutosApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatalogoProdutosApiApplication.class, args);
	}

}
