package com.im.catalogoprodutos.api.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import lombok.Data;


@Data
public class ProdutoInputDTO {

	@NotBlank
	private String name;
	
	@NotBlank
	private String description;
	
	@NotNull
	@PositiveOrZero
	private BigDecimal price;
	
}
