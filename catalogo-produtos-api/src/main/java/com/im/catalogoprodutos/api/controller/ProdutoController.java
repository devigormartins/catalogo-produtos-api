package com.im.catalogoprodutos.api.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.im.catalogoprodutos.api.dto.ProdutoDTO;
import com.im.catalogoprodutos.api.dto.ProdutoInputDTO;
import com.im.catalogoprodutos.domain.model.Produto;
import com.im.catalogoprodutos.domain.repository.ProdutoRepository;
import com.im.catalogoprodutos.domain.service.ProdutoService;

@RestController
@RequestMapping("/products")
public class ProdutoController {
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping
	public List<ProdutoDTO> listar() {
		List<Produto> produtos = produtoRepository.findAll();
		
		return toCollectionModel(produtos);
	}
	
	@PostMapping
	public ResponseEntity<ProdutoDTO> adicionar(@Valid @RequestBody ProdutoInputDTO produto){
		Produto produtoSalvo = produtoService.salvar(toEntity(produto));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(toModel(produtoSalvo));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<ProdutoDTO> atualizar(@Valid @PathVariable Long id, @Valid @RequestBody ProdutoInputDTO produtoDto){
		if(!produtoRepository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		
		Produto produto = new Produto();
		produto = toEntity(produtoDto);
		produto.setId(id);
		produto = produtoService.salvar(produto);
		
		return ResponseEntity.ok(toModel(produto));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProdutoDTO> buscar(@PathVariable Long id){
		if(!produtoRepository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		Optional<Produto> produto = produtoRepository.findById(id);
		
		return ResponseEntity.ok(toModel(produto.get()));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> excluir(@PathVariable Long id){
		if(!produtoRepository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		
		produtoService.excluir(id);
		
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<ProdutoDTO>> buscaComFiltro(@RequestParam(name = "q", required = false) String nomeOuDescricao, 
			@RequestParam(name = "min_price", required = false) BigDecimal precoMinimo, @RequestParam(name = "max_price", required = false) BigDecimal precoMaximo){
		List<Produto> produtos = produtoRepository.consultaComFiltros(nomeOuDescricao, precoMinimo, precoMaximo);
		
		return ResponseEntity.ok(toCollectionModel(produtos));
	}
	
	private ProdutoDTO toModel(Produto produto) {
		return modelMapper.map(produto, ProdutoDTO.class);
	}
	
	private Produto toEntity(ProdutoInputDTO produtoInput) {
		return modelMapper.map(produtoInput, Produto.class);
	}
	
	private List<ProdutoDTO> toCollectionModel(List<Produto> produtos){
		return produtos.stream()
							.map(produto -> toModel(produto))
							.collect(Collectors.toList());							
	}

}
