package com.im.catalogoprodutos.api.dto;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
public class ProdutoDTO {

	@EqualsAndHashCode.Include
	private Long id;	
	private String name;	
	private String description;	
	private BigDecimal price;
	
}
