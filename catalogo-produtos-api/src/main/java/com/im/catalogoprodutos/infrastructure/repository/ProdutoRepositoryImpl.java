package com.im.catalogoprodutos.infrastructure.repository;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.im.catalogoprodutos.domain.model.Produto;
import com.im.catalogoprodutos.domain.repository.ProdutoRepositoryQueries;

@Repository
public class ProdutoRepositoryImpl implements ProdutoRepositoryQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public List<Produto> consultaComFiltros(String nomeOuDescricao, BigDecimal precoMinimo, BigDecimal precoMaximo) {
		var jpql = new StringBuilder();		
		jpql.append("from Produto where 1 = 1 ");
		
		var parametros = new HashMap<String, Object>();
		
		if(StringUtils.hasLength(nomeOuDescricao)) {
			jpql.append(" and (name like :nomeOuDescricao or description like :nomeOuDescricao) ");
			parametros.put("nomeOuDescricao", "%" + nomeOuDescricao + "%");
		}
		
		if(precoMinimo != null) {
			jpql.append(" and price >= :precoMinimo ");
			parametros.put("precoMinimo", precoMinimo);
		}
		
		if(precoMaximo != null) {
			jpql.append(" and price <= :precoMaximo");
			parametros.put("precoMaximo", precoMaximo);
		}
		
		TypedQuery<Produto> query = manager.createQuery(jpql.toString(), Produto.class);
		
		parametros.forEach((chave, valor) -> query.setParameter(chave, valor));
		
		return query.getResultList();
	}

}
